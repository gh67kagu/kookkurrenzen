import os
import os.path
import pandas as pd
import argparse
import matplotlib.pyplot as plt

def import_csv(input_folder, filename_infix):
    csv_files = {}
    for (dirpath, _, filenames) in os.walk(input_folder):
        for filename in filenames:
            if not filename_infix in filename:
                continue
            (_, extension) = os.path.splitext(filename)
            if extension == ".csv":
                full_file_path = os.path.join(dirpath, filename)
                label = os.path.split(os.path.dirname(full_file_path))[-1]
                csv_files[label] = pd.read_csv(full_file_path)

    return csv_files

def cosine_similarity(first_csv, second_csv):
    def magnitude(csv):
        return sum([x**2 for x in csv["V3"].values]) ** 0.5

    def get_dict(csv):
        result = {}

        for index, row in csv.iterrows():
            result[row["V2"]] = row["V3"]

        return result

    first_dict = get_dict(first_csv)
    second_dict = get_dict(second_csv)
    common_words = set(first_dict.keys()) & set(second_dict.keys())

    dot_product = 0
    for word in common_words:
        dot_product += first_dict[word] * second_dict[word]
    
    return dot_product / (magnitude(first_csv) * magnitude(second_csv))

# Cosine similarity größer bei weniger Veränderung

def cosine_distance(first_csv, second_csv):
    return 1 - cosine_similarity(first_csv, second_csv)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, default="", help="Path to input data")
    parser.add_argument("-d", "--dice", action="store_true", default=False, help="Include this flag for dice measure")
    parser.add_argument("-l", "--log", action="store_true", default=False, help="Include this flag for log likelihood measure")
    parser.add_argument("-m", "--mi", action="store_true", default=False, help="Include this flag for mutual information")
    parser.add_argument("-o", "--output", type=str, default="./results.txt", help = "Path to results file")
    args = parser.parse_args()

    input_folder = args.input # ".\Bundestag"
    outfilepath = args.output

    if args.dice:
        measure = 'dice'
    elif args.log:
        measure = 'log'
    elif args.mi:
        measure = 'mi'

    stuff = import_csv(input_folder, measure)

    sorted_labels = sorted(stuff.keys()) # [1999-2000, 2001-2002 ..]
    clist = []
    for first_label, second_label in zip(sorted_labels, sorted_labels[1:]):
        first_csv = stuff[first_label]
        second_csv = stuff[second_label]
        cdist = cosine_distance(first_csv, second_csv)
        clist.append(cdist)
        print(f"cosine similarity between {first_label} and {second_label}: {cdist}")

    cdict = dict(zip(sorted_labels, clist))

    f = open(outfilepath, "w")
    f.write("Results for " + measure + ": \n\n" + str(cdict))
    f.close()

    plt.plot([int(l.split('-')[0]) for l in sorted_labels[:-1]], clist)
    plt.xlabel('Distanz zwischen Zweijahresschritten (Jahr + 3)')
    plt.ylabel('Kosinusdistanz')
    plt.title('Kosinusdistanz für \'nachhaltig\'')
    plt.show()